package com.sbux.gsit.azureconntest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HomeController {

	@Autowired
	JdbcTemplate template;
	
	
	
	@GetMapping
	public Integer get() {
		int count = template.queryForObject("SELECT count(*) FROM XXOES_PROD_TXN_SUB", Integer.class);
		return count;
	}
	
	@GetMapping("google")
	public String get1() {
		Map map = new HashMap();
		
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<String> response = restTemplate.exchange("https://randomuser.me/api/?nat=US", HttpMethod.GET, null, String.class, map);
		return response.getBody();
	}
	
}
