Objective
=========
The objective of this code is to test database connection on microsoft azure east and west database center with SSL leaf, intermediate and root certificate

Prerequisite
============
1. Install [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) if not installed 
2. Install [GIT Bash](https://git-scm.com/downloads) if you comfortable with git commands or download [sourcetree](https://www.sourcetreeapp.com/) for git UI.
3. Install [maven](https://maven.apache.org/download.cgi). here are install [instructions](https://maven.apache.org/install.html)
4. Optional Install spring tool suite([STS](https://spring.io/tools/sts/all)) if you want to import your code in an IDE or your favorite IDE.   
5. Create a SQL server instance on [west](westus1-a.control.database.windows.net) and it fall over instances on [east](cr2.eastus1-a.control.database.windows.net) azure datacenter, keep database instance name and credentails same for both.
6. Create a logical grouping URL of both the instances.
6. create a table XXOES_PROD_TXN_SUB with probably minimum one column of any name
7. Insert few records in that table.


Guidelines
=========
1. Open command prompt and change directory to your desired directory.
2. git clone https://bitbucket.org/amitamit281/azureconntest
    or 
    checkout in sourcetree.
3. Import that project into STS IDE as maven project.
4. Change application.properties, with your url, database instance name and credentials.
5. Open command prompt and chnage directory to project folder
6. mvn spring-boot:run -Djavax.net.ssl.trustStore=src/main/resources/east-cacerts.jks "-Djavax.net.ssl.trustStorePassword=changeit"
7. open a browser and hit http://localhost:8080/, it'll show you number records inserted into table. else SSL exception.


Issue
=====
when west coast site is up it works fine with leaf certificate but if east is up and west is down, didn’t work neither of east leaf and intermediate. But works with root cert.
 
My two cents for leaf certificate why didn’t work
ping s00326fogoescert.database.windows.net
PING cr2.eastus1-a.control.database.windows.net (40.121.158.30): 56 data bytes
it resolves prefixed with “cr2” but if you look at east leaf cert its subject is eastus1-a.control.database.windows.net which does not match with the resolved host name and this is not wild card certificate that might be the reason leaf cert didn’t work. But don’t know for intermediate cert.

Exception
=========
Error we get for both certs
javax.net.ssl.SSLHandshakeException: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
 